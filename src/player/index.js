import React, {useEffect, useState, useRef} from 'react';
import {WS_REGISTRY_POINT, WS_SERVER,} from '../constants';
import '../css/index.css';
import SockJsClient, {} from 'react-stomp';
import {Button, Container, Row, Col} from 'react-bootstrap';

// @import "~bootstrap/scss/bootstrap";

const Player = () => {
	
	const [connected, setConnected] = useState(false);
	const refContainer = useRef(undefined);
	useEffect(() => {
	    
	    console.log('refContainer ::::', refContainer);
	    if(refContainer?.current?.client){
	        setConnected(true);
          refContainer.subscribe();
          initializePlayer();
      }
	}, []);
	
	
	function initializePlayer() {
		
		const script = document.createElement('script');
		script.async = true;
		script.src = 'http://www.youtube.com/iframe_api/';
		//For head
		document.head.appendChild(script);
		// For body
		document.body.appendChild(script);
		script.onload = () => onYouTubeIframeAPIReady();
		// For component
	}
	//Ci0WbaUH3no
	function onYouTubeIframeAPIReady() {
		const player = new window.YT.Player('ytplayer', {
			height: '390',
			width: '640',
			videoId: 'nxsfFB-cOMI',
			allow: 'autoplay',
			playerVars: {
				'rel': 0,
				'modestbranding': 1,
				'autohide': 1,
				'mute': 0,
				'showinfo': 0,
				'controls': 0,
				'autoplay': 0,
			},
			events: {
				'onReady': onPlayerReady,
			}
		});
	}
	
	function onPlayerReady(event) {
		event.target.unMute();
	}
	
	return (
		<div>
        <Container fluid={true}>
            <Row>
               <Col md={12} lg={12}>
               
               </Col>
            </Row>
            <Row>
                <Col md={12} lg={12}>
                    <div id="ytplayer">
                        Client is not connected.
                    </div>
                </Col>
            </Row>
            <Row>
                <Col md={12} lg={12}>
                    <Button variant="success">Start</Button>{' '}
                    <Button variant="danger">Stop</Button>
                </Col>
            </Row>
        </Container>
			
			
			<SockJsClient url={`${WS_SERVER}${WS_REGISTRY_POINT}`} topics={['/topic/changes']}
			              onMessage={(msg) => {
				              console.log(msg);
			              }}
			              ref={refContainer}/>
		</div>
	);
};


export default React.memo(Player);
