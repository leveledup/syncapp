package com.demo.syncapp.enums;

import java.io.Serializable;

public enum RequestType implements Serializable {

    START,
    RESUME,
    PAUSE,
    RESTART,
    MOVE
}
