package com.demo.syncapp.dto;

import com.demo.syncapp.enums.RequestType;

import java.io.Serializable;

public class Request implements Serializable {

    private RequestType type;
    private Double seconds;

    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    public Double getSeconds() {
        return seconds;
    }

    public void setSeconds(Double seconds) {
        this.seconds = seconds;
    }
}
