package com.demo.syncapp.controller;


import com.demo.syncapp.dto.Request;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class GreetingController {


    @MessageMapping("/proceed")
    @SendTo("/topic/changes")
    public Request greeting(Request message){
        return message;
    }


    @PostMapping
    @SendTo("/topic/changes")
    public Request sendData(@RequestBody Request message){
        return message;
    }


}